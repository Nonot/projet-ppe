<?php session_start();
require_once 'fonctions/erreurs.php';
?>
<!DOCTYPE html>
<html lang="en" id="fond">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script type="text/javascript" src="scripts/theme.js"></script>
  <script type="text/javascript" src="scripts/demandes.js"></script>


  <link rel="stylesheet" type="text/css" href="styles/style_dark.css">
  <link rel="stylesheet" type="text/css" href="styles/style_bright.css">
  <link rel="stylesheet" type="text/css" href="styles/style.css">
  <link rel="shortcut icon" href="images/icon.png">
  <!-- <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> -->
</head>

<body onload="checkCookie(); loadBkg();">
<div class="page" id="page">
    <?php  require_once 'controleur/controleurPrincipal.php'; ?>
    <footer id="footer">
      <div class="Signature">
        <p>
          Site crée par Edouard Monimaud et Arnaud Tardif
        </p>
      </div>
      <div class="onoffswitch" onload="chgtBkg()">
        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" onChange="setBkg()">
        <label class="onoffswitch-label" for="myonoffswitch"></label>
      </div>

      <div class="Liens">
        <p>
          <a href="mailto:contact@nonot.eu">Contact</a>
          <a href="http://www.eiffel-bordeaux.org/">Lycée Eiffel</a>
        </p>
      </div>
    </footer>
  </div>

</body>
<head>
<title><?php echo $title?> - Gestion des incidents informatiques</title>
</head>
</html>
