// Change le background si le switch est cliqué
function setBkg() {
    if (document.getElementById('myonoffswitch').checked == true) {
        setCookie("theme","dark",365);
    }
    else {
        setCookie("theme","bright",365);
    }

    for ( i=0; i<document.styleSheets.length; i++) {
        if (document.getElementById('myonoffswitch').checked == false) { //théme clair
            if(document.styleSheets.item(i).href == "https://ppe.nonot.eu/styles/style_dark.css"){
                document.styleSheets.item(i).disabled=true;
            }
            else{
                document.styleSheets.item(i).disabled=false;
            }
        }
        else{ // théme sombre
            if(document.styleSheets.item(i).href == "https://ppe.nonot.eu/styles/style_bright.css"){
                document.styleSheets.item(i).disabled=true;
            }
            else{
                document.styleSheets.item(i).disabled=false;
            }
        }
    }
}

// au chargement de la page attribue les classes en fonction de l'état du switch
function loadBkg(){
    for ( i=0; i<document.styleSheets.length; i++) {
        if (document.getElementById('myonoffswitch').checked == false) { //théme clair
            if(document.styleSheets.item(i).href == "https://ppe.nonot.eu/styles/style_dark.css"){
                document.styleSheets.item(i).disabled=true;
            }
            else{
                document.styleSheets.item(i).disabled=false;
            }
        }
        else{ // théme sombre
            if(document.styleSheets.item(i).href == "https://ppe.nonot.eu/styles/style_bright.css"){
                document.styleSheets.item(i).disabled=true;
            }
            else{
                document.styleSheets.item(i).disabled=false;
            }
        }
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//exécuté au chargement de la page pour checked ou non le switch OU créer le cookie
function checkCookie() {
    var theme = getCookie("theme");
    if (theme != "") {
        if (getCookie("theme") == "dark") {
            document.getElementById("myonoffswitch").checked = true;
        }
        else if (getCookie("theme") == "bright") {
            document.getElementById("myonoffswitch").checked = false;
        }
    }
    else {
        setCookie("theme","bright",365);
    }
}
