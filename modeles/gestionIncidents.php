<?php
include 'acces/accesDonnees.php';


//#######################################################################
//insére dans la bdd une demande utilisateur
//#######################################################################

function creerDemande($materiel, $resume){
    //echo $materiel;
    $materiel= htmlspecialchars($materiel);
    $resume= htmlspecialchars($resume);
    include 'acces/accesDonnees.php';
    $login=$_SESSION['identifiant'];
    $date=date("Y-m-d");
    $statut="AT";
    try {
        $req = $bdd->prepare("INSERT INTO incident (LoginUtilisateur,DateIncident,Resume,Statut,NomMateriel) VALUES (?, ?, ?, ?, ?)");
        $req->execute(array($login,$date,$resume,$statut,$materiel));
    }catch(PDOException $e){
        echo $e->getMessage();
    }

}


function bddDonneesProfil($login){

    include 'acces/accesDonnees.php';
    $reponse = $bdd->prepare('SELECT * FROM utilisateur WHERE Identifiant = ?');
    $reponse->execute(array($login));
    Return $reponse->fetch();
}

function bddDemandesUtil(){
    include 'acces/accesDonnees.php';
    $reponse = $bdd->prepare('SELECT NumeroIncident as "#", LoginUtilisateur as "Login", NomMateriel as "Matériel", DateIncident as "Date", Resume as "Description", Statut  FROM incident WHERE LoginUtilisateur = ? ORDER BY DateIncident DESC');
    $reponse->execute(array($_SESSION['identifiant']));
    Return $reponse->fetchall(PDO::FETCH_ASSOC);
}

function bddDemandesAdmin(){
    include 'acces/accesDonnees.php';
    $reponse = $bdd->prepare('SELECT NumeroIncident as "#", LoginUtilisateur as "Login", NomMateriel as "Matériel", DateIncident as "Date", Resume as "Description", Statut  FROM incident ORDER BY DateIncident DESC');
    $reponse->execute();
    Return $reponse->fetchall(PDO::FETCH_ASSOC);
}

function bddMateriel(){
    include 'acces/accesDonnees.php';
    $req = $bdd->prepare("SELECT Distinct Modele, Nom FROM materiel ORDER BY Modele");
    $req->execute();
    Return $req->fetchall(PDO::FETCH_NUM);
}

function bddGetModeleByNom($materiel){
    include 'acces/accesDonnees.php';
    $req = $bdd->prepare("SELECT Modele FROM materiel WHERE Nom = ?");
    $req->execute(array($materiel));
    Return $req->fetch();
}
function bddNomExisteDeja($id){
    include 'acces/accesDonnees.php';
    $req = $bdd->prepare("SELECT Nom FROM materiel");
    $req->execute(array());
    $liste = $req->fetchall();
    foreach ($liste as $key => $value) {
        if($value[0] == $id){
            Return True;
        }
    }
    Return False;
}

function bddAjouterMateriel($liste){
    include 'acces/accesDonnees.php';
    $Nom=htmlspecialchars($liste['Nom']);
    $Modele=htmlspecialchars($liste['Modele']);
    $Lieu=htmlspecialchars($liste['Lieu']);
    $Fabricant=htmlspecialchars($liste['Fabricant']);
    $IP=htmlspecialchars($liste['IP']);
    $Type=htmlspecialchars($liste['Type']);

    try {
        $req = $bdd->prepare("INSERT INTO materiel (`Nom`,`Fabricant`,`Modele`,`Lieu`,`IP`,`Type`) VALUES (?, ?, ?, ?, ?, ?)");
        $req->execute(array($Nom,$Fabricant,$Modele,$Lieu,$IP,$Type));
        Return True;
    }catch(PDOException $e){
        Return False;
    }
}

function validerDemandes($demandes){
    include 'acces/accesDonnees.php';
    foreach ($demandes as $key => $value) {
        if($key != "gestion"){
            $num = strval($key);
            try {
                $req = $bdd->prepare("UPDATE incident SET Statut = 'R' WHERE NumeroIncident = ?");
                $req->execute(array($num));

            }
            catch(PDOException $e)
            {
                echo $req . "<br>" . $e->getMessage();
            }
        }
    }
}

function getInfosDemands($id){

  include 'acces/accesDonnees.php';
  $req = $bdd->prepare('SELECT LoginUtilisateur, Resume, NomMateriel FROM incident WHERE NumeroIncident = ?');
  $req->execute(array($id));

  Return $req->fetch(PDO::FETCH_ASSOC);
}

function getMailAddr($id){
  include 'acces/accesDonnees.php';
  $req = $bdd->prepare('SELECT Messagerie FROM utilisateur WHERE Identifiant = ?');
  $req->execute(array($id));
  $mail = $req->fetch(PDO::FETCH_NUM);
  Return strval($mail[0]);
}
?>
