<?php
//AFFICHE LA SYNTHESE DU PRÉCÉDENT AJOUT SUCCÈS OU NON
if ($_SESSION['profil'] == "administrateur" && isset($_POST['ajouter'])) {
  $synthese = ajouterMateriel($_POST);
  include 'vues/administrateurSyntheseAjout.php';
}
//AFFICHE LE FORMULAIRE D'AJOUT
if ($_SESSION['profil'] == "administrateur") {
  if (!isset($synthese)) {
    $synthese = "";
  }
  include 'vues/administrateurAjouterMateriel.php';
}
$title = "Ajout de matériel";
?>
