<?php

require_once 'modeles/gestionIncidents.php';
require_once 'fonctions/fonctions.php';

//##############################################################################
//GESTION DES POST
//##############################################################################

//détruit la session
if(isset($_GET['page']) && $_GET['page'] == "deco"){
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
$_SESSION = array();
session_destroy();
}

//charge les pages GET en SESSION pour traitement ultérieur
//initialise la page d'accueil si défaut de choix
elseif (isset($_GET['page'])) {
    $_SESSION['page'] = $_GET['page'];
}

if(!isset($_SESSION['page'])){
    $_SESSION['page'] = "accueil";
}


//##############################################################################
//établit une connexion si des ids sont données
//##############################################################################
if(isset($_POST['id']) && isset($_POST['mdp']) ){
    etablirConnexion($_POST['id'], $_POST['mdp'] );
}
//##############################################################################
//affiche l'entete de la page
//##############################################################################
require_once 'vues/entetes.php';
?>

<div class="corps" id="corps">

    <?php
    //##############################################################################
    //Gestion de l'affichage des contenus des pages
    //##############################################################################
    if (!isset($_SESSION['identifiant'])) {
        require_once 'vues/interfaceConnexion.php';
				$title = "Connexion";
    }
    else{
        if($_SESSION['page'] == "accueil"){
            include 'controleur/accueil.php';
        }
        elseif ($_SESSION['page'] == "visualiserDemandes") {
            include 'controleur/visualiserDemandes.php';
        }
        elseif ($_SESSION['page'] == "formulaireDemande") {
            $_SESSION['POST']=$_POST;
            include 'controleur/formulaireDemande.php';

        }
        elseif ($_SESSION['page'] == "ajouterMateriel") {
            include 'controleur/ajouterMateriel.php';
        }
    }
    ?>
</div>
<footer></footer>
