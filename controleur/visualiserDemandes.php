<?php
  require_once 'fonctions/fonctions.php';
  //AFFICHE LES DEMANDES AVEC CHECKBOX POUR LES ADMINISTRATEURS
  if(isset($_POST['gestion']) && $_SESSION['profil'] == "administrateur"){ 
      validerDemandes($_POST);
      $liensMails = generationHTMLmails(generationTabloMails($_POST));
  }
  else {
    $liensMails = "";
  }
  if ($_SESSION['profil'] == "utilisateur") {
      $demandes = recupDemandes();
      include 'vues/utilisateurVoirDemandes.php';
  }
  elseif ($_SESSION['profil'] == "administrateur") {
      $demandes = recupDemandes();
      if ($liensMails!="") {
        include 'vues/administrateurSyntheseGestionDemandes.php';
      }
      include 'vues/administrateurGestionDemandes.php';


  }
  $title = "Visualisation des demandes";
?>
