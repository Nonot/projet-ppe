<?php
require_once 'fonctions/fonctions.php';
//AFFICHE LA PAGE DE SOUMISSION DE DEMANDES
if(!isset($_POST['soumettre']) || $_POST['resume'] == "" ){
$selected="";
$formulaire=recupererMateriel("materiel", $selected);
include 'vues/utilisateurSignalerIncident.php';
}
//SINON AFFICHE LA PAGE DE SYNTHÉSE DE LA DEMANDE EFFECTUÉ
else{
  $materiel=htmlspecialchars($_POST['materiel']);
  $resume=htmlspecialchars($_POST['resume']);
  creerDemande($materiel,$resume);
  $synthese = creerTabloSynthese($materiel,$resume);
  include 'vues/utilisateurSyntheseIncident.php';
}
$title = "Création de demande";
?>
