<!-- BANDEAU DE NAVIGATION -->
<!-- UN FORM SERVANT À REDIRIGER VERS UNE DÉCONNEXION -->

<div class="menu">
    <!-- LIEN VERS L'ACCUEIL -->
  <div class="home">
    <a href='index.php?page=accueil'>
      <p>Accueil</p>
    </a>
  </div>
  <!-- MESSAGE DE BIENVENUE -->
  <div class="bienvenue">
    <p>Bonjour <?php echo $_SESSION['nom']?></p>
  </div>
  <!-- LIEN POUR LA DÉCONNEXION -->
  <div class="deco">
    <a href="index.php?page=deco">
      <p>Déconnexion</p>
    </a>
  </div>
</div>
