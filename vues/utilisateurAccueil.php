<!-- ACCUEIL DE L'UTILISATEUR -->
<fieldset>
    <legend>
        <h1>Bienvenue <?php echo $_SESSION['nom']; ?></h1>
    </legend>
    <h2>Que souhaitez-vous faire?</h2>
    <h3><a href="index.php?page=visualiserDemandes">Visualiser l'état d'avancement des incidents déjà signalés</a></h3>

    <h3><a href="index.php?page=formulaireDemande">Signaler un nouvel incident</a></h3>
</fieldset>
