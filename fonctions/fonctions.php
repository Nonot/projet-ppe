<?php

//#######################################################################
// établir la session
//#######################################################################
function etablirConnexion($login, $password){
    $row = bddDonneesProfil(htmlspecialchars($login));

    $mdp = $row['MotDePasse'];

    if ( ($password === $mdp) &&  ($row['Actif'] === "Oui") ) {

        $_SESSION['identifiant'] = $row['Identifiant'];
        $_SESSION['nom'] = $row['Nom'];
        $_SESSION['messagerie'] = $row['Messagerie'];
        $_SESSION['lieu'] = $row['Lieu'];
        $_SESSION['profil'] = $row['Profil'];
        $_SESSION['page'] = "accueil";

        $actif = $row['Actif'];
        $profil = $row['Profil'];
    }
    else{
        global $erreurConnexion;
        $erreurConnexion = "Le couple Identifiant / Mot de passe n'est pas valide.<br/><br/>";
    }
}

//#######################################################################
//recuper les demandes utilisateur
//#######################################################################

function recupDemandes(){
    if ($_SESSION['profil'] == "administrateur") {
        $rows = bddDemandesAdmin();
    }
    elseif ($_SESSION['profil'] == "utilisateur") {
        $rows = bddDemandesUtil();
    }
    if(count($rows) !== 0){
        if(count($rows) !== 0){
            $tablo="";
            $tablo.= "<table class=\"demandes\">";
            $tablo.= "<thead><tr>";
            // add the table headers
            foreach ($rows[0] as $key => $useless){
                $tablo.= "<th class='demandes".$key."'>$key</th>";
            }
            $tablo.= "</tr></thead>";
            // display data
            foreach ($rows as $row){
                // attribue la class resolved si la demande l'est
                $tablo.= "<tr";
                if ($row['Statut']== "R") {
                    $tablo .= " class='resolved'";
                }

                $tablo .= ">";

                //On n'utilise pas un foreach pour parcourir les $value car ils faut appliquer une classe spécifiques à chaque <td>
                $tablo .= "<td class='demandes#'>".$row['#']."</td>";
                $tablo .= "<td class='demandesLogin'>".$row['Login']."</td>";
                $tablo .= "<td class='demandesMatériel'>".$row['Matériel']."</td>";
                $tablo .= "<td class='demandesDate'>".$row['Date']."</td>";
                $tablo .= "<td class='demandesDescription'>".$row['Description']."</td>";
                $tablo .= "<td class='demandesStatut'>";
                if ($row['Statut'] == "AT" && $_SESSION['profil'] == "administrateur") {
                    $tablo .= "<input type='checkbox' id='".$row['#']."' name='".$row['#']."'/>";
                }
                else {
                    $tablo .= $row['Statut'];
                }
                $tablo .= "</td>";
                $tablo.= "</tr>";
            }
            // close the table
            $tablo.= "</table><br/>";
        }
    }
    else {
        $tablo = "Vous n'avez pas de demandes en cours.";
    }
    Return $tablo;
}

//#######################################################################
//Update les demandes marqués comme résolu par l'administrateur
//#######################################################################
function generationTabloMails($demandes){
    $mails = [];

    foreach ($demandes as $key => $value) {
        if($key != "gestion"){
            $num = strval($key);
            $details = getInfosDemands($key);

            $login = $details['LoginUtilisateur'];
            $resume = $details['Resume'];
            $matériel = $details['NomMateriel'];
            $message = "Votre demande numéro ".$key." a été signalé comme résolu.%0D%0AMatériel : ".$matériel."%0D%0AResume : ".$resume."%0D%0A%0D%0A";
            if (array_key_exists($login,$mails)) {
                $mails[$login] .= $message;
            }
            else {
                $mails[$login] = $message;
            }
        }
    }
    Return $mails;
}

//#######################################################################
//génére des liens mailto à partir d'un tableau
//#######################################################################
function generationHTMLmails($mails){
    $html = "";
    foreach ($mails as $key => $value) {
        $adrMail = getMailAddr($key);
        $html .= '<a href="mailto:'.$adrMail.'?subject=Résolution de demande(s)&body='.$value.'">'.$key.'</a><br/>';
    }
    Return $html;
}

//#######################################################################
//récupérer les appareils dans la bdd
//#######################################################################
function recupererMateriel($name){
    $materiel = bddMateriel();

    Return ajouterSelect($name,$name, $materiel, "");
}

//#######################################################################
//renvoi une liste déroulante
//#######################################################################
function ajouterSelect($id, $name, $tablo, $selected){

    $select = "<select id='".$id."' name='".$name."'>";
    foreach ($tablo as $ligne) {
        $select .= "<option value='";
        $select .= $ligne[1]."' ";

        if ($ligne[0] == $selected) {
            $select .= "selected ='selected' ";
        }
        $select .= ">";
        $select .= $ligne[0].", ".$ligne[1]." </option>";
    }
    $select .= "</select>";
    Return $select;
}

//#######################################################################
//renvoi le tablo de la demande utilisateur
//#######################################################################
function creerTabloSynthese($materiel, $resume){

    $modele = bddGetModeleByNom($materiel);
    $modele = $modele[0];
    $tablo = "";
    $tablo .= "<table class='demandes'><tr><td>Date de l'incident</td><td>";
    $tablo .=date("Y-m-d");
    $tablo .="</td></tr><tr><td>Materiel</td><td>";
    $tablo .=$modele;
    $tablo .="</td></tr><tr><td>Resumé</td><td>";
    $tablo .=$resume;
    $tablo .="</td></tr></table>";
    Return $tablo;
}

//#######################################################################
//Ajoute le matériel si pas de doublons sur la clé primaire,
//si tout les champs sont remplis
//si erreur, message apparait
//#######################################################################
function ajouterMateriel($liste){
    require_once 'acces/accesDonnees.php';
    require_once 'modeles/gestionIncidents.php';
    if (bddNomExisteDeja($liste['Nom']) === True) {
        Return "Ajout impossible ".$liste['Nom']." existe déjà";
    }
    //si pas toutes les données
    foreach ($liste as $key => $value) {
        if($value ==""){
            Return "Ajout impossible \"".$key."\" manquant";
        }
    }
    //si ID pas 8 caractéres
    if(strlen($liste['Nom']) > 8){
        Return "Le nom ne doit pas dépasser 8 caractéres";
    }
    if (bddAjouterMateriel($liste) === True) {
        $retour = "<p>Ajouté avec succès</p><table class=''>";
        foreach ($liste as $key => $value) {
            $retour.="<tr><th>".$key."</th><td>".$value."</td></tr>";
        }
        $retour.="</table>";
        Return $retour;
    }
    else {
        Return "Une erreur inconnue est survenu \nInfo sur la requete :\n".var_dump($_POST);
    }
    //
    Return "string";
}
?>
